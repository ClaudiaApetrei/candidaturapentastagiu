import java.util.ArrayList;
import java.util.List;
public class Prim {
	private List<Integer> primeSumList = new ArrayList<Integer>(); //lista nr prime testate pentru interval
	private List<Integer> primeSumTerms = new ArrayList<Integer>();//lista nr de termeni din fiecare suma
	private List<Integer> primeNrList = new ArrayList<Integer>();//toate nr prime de la 0 pana la max;

	public boolean IsPrime(int number) {
		int limit = (int) Math.floor(Math.sqrt(number));

		if (number == 1 || number == 0)
			return false;
		if (number == 2)
			return true;

		for (int i = 2; i <= limit; ++i) {
			if (number % i == 0)
				return false;
		}

		return true;
	}

	public void LargestPrime(int min, int max) {
		if (max >= 2) {
			int sumaPrim = 0;
			int k, count, minAux = 0;
			for (int i = 0; i <= max; i++) {
				if (IsPrime(i)) {
					primeNrList.add(i);
				}
			}
			//urmatorul numar prim dupa valoarea minima din interval
			for (int i = min; i <= max; i++) {
				if (IsPrime(i)) {
					minAux = i;
					break;
				}
			}

			for (int i = primeNrList.indexOf(minAux); i < primeNrList.size(); i++) {
				k = 0;

				for (int j = 0; j < i + 1; j++) {
					k = j;
					sumaPrim = 0;
					count = 0;

					while (sumaPrim < primeNrList.get(i)) {
						sumaPrim += primeNrList.get(k);
						k++;
						count++;//incrementez dimensiunea termenilor sumei
					}
					if (sumaPrim == primeNrList.get(i)) {
						primeSumList.add(sumaPrim);
						primeSumTerms.add(count);
						break;
					}
				}
			}
		} else {
			System.err.println("Nu exista numere prime pentru acest interval");
			System.exit(0);
		}
	}

	public List<Integer> getPrimeSumList() {
		return primeSumList;
	}

	public List<Integer> getprimeSumTerms() {
		return primeSumTerms;
	}

}


